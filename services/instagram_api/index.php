<?php

/**
   * The call to the API.
   *
   * @param string $endpoint              API resource path
   * @return json
   */
function callApi($api_endpoint = '') 
{
    $auth_method = '?client_id=' . CLIENT_ID;
    
	$api_call = $api_endpoint . $auth_method ;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $api_call);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
    curl_setopt($ch, CURLOPT_TIMEOUT, 360);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $json_data = curl_exec($ch);

    // split header from JSON data
    // and assign each to a variable
    list($header_content, $json) = explode("\r\n\r\n", $json_data, 2);

    if (!$json) {
      throw new Exception("Error: callApi() - cURL error: " . curl_error($ch));
    }

    curl_close($ch);

    return json_decode($json);
}



# include the settings
require_once("config.php");



# get the keywords(tags)
$tags = $_GET['keywords'];
$keywords = array_filter( explode(',', $tags) );

if(is_array($keywords) && !empty($keywords)) 
{
	# loop through the tags to fetch data from API
	foreach ($keywords as $tag) 
	{
		$keyword_containers[$tag] = array();

		// fetch url
		$tag_media_recent_uri = TAG_ENDPOINT."$tag/media/recent";
		$tag_response = callApi($tag_media_recent_uri);
		
		if(is_array($tag_response->data) && !empty($tag_response->data)) 
		{
			foreach($tag_response->data as $object)
			{
				$tag_object = [
					 "type"	=> $object->type	
					,"username"	=> $object->user->username
					,"profile_picture"	=> $object->user->profile_picture
					,"full_name"	=> $object->user->full_name
					,"url"	=> $object->images->standard_resolution->url
					,"width"	=> $object->images->standard_resolution->width
					,"height"	=> $object->images->standard_resolution->height
					,"thumbnail_url"	=> $object->images->thumbnail->url
					,"thumbnail_width"	=> $object->images->thumbnail->width
					,"thumbnail_height"	=> $object->images->thumbnail->height
				];

				$keyword_containers[$tag][] = $tag_object;	
			}
		}
			
	}
}
else 
{
	echo 'No Keywords Provided.';
}


/* Iterate over the keyword_containers to intermix the results */

# get the total number of keywords
$num_keywords = count($keywords);
$keyword_index = 0;
$result_collection = array();

$item = array_shift($keyword_containers[$keywords[$keyword_index]]);
do 
{

	# add the item to the result collection 
	if( ! is_null($item) ) $result_collection[] = $item;

	#increment the keyword index
	$keyword_index++;

	# if index has fallen out-of-bound, reset it to the first keyword_index 
	if($keyword_index >= $num_keywords ) 
		$keyword_index = 0;

	# set the next item
	$item = array_shift($keyword_containers[$keywords[$keyword_index]]);

	# if a particular keyword array is now empty, then we should remove it
	if( count($keyword_containers[$keywords[$keyword_index]]) == 0 ) 
	{
		# remove the keyword from the keywords array
		unset($keywords[$keyword_index]);
		$keywords = array_values($keywords);

		# reset the number of keywords after removing one
		$num_keywords = count($keywords);
	}
}while( $num_keywords > 0 );

# print the output as array in readable format on browser
//echo '<pre>';print_r($result_collection);

# print output as JSON object.
echo json_encode($result_collection);